@ECHO OFF
setlocal

set PROJECT_HOME=%~dp0
set DEMO=Fluent Bit Easy Install demo
set AUTHORS=Eric D. Schabell
set PROJECT=git@gitlab.com:o11y-workshops/fluentbit-install-demo.git
set FLUENTBIT_VERSION=3.2.2
set FLUENTBIT_SRC=fluent-bit-%FLUENTBIT_VERSION%.zip
set FLUENTBIT_HOME=.\target\fluent-bit-%FLUENTBIT_VERSION%
set BISON_VERSION=3

REM wipe screen.
cls

echo
echo Windows install support coming soon...
echo
exit;

echo.
echo ################################################################
echo ##                                                            ##
echo ##  Setting up the %DEMO%               ##
echo ##                                                            ##
echo ##      ##### #   # ##### #   # #####   ####  ##### #####     ##
echo ##      #     #   # #     ##  #   #     #   #   #     #       ##
echo ##      ####  #   # ###   # # #   #     ####    #     #       ##
echo ##      #     #   # #     #  ##   #     #   #   #     #       ##
echo ##      #     ##### ##### #   #   #     ####  #####   #       ##
echo ##                                                            ##
echo ##                #####  ###   #### #   #                     ##
echo ##                #     #   # #      # #                      ##
echo ##                ###   #####  ###    #                       ##
echo ##                #     #   #     #   #                       ##
echo ##                ##### #   # ####    #                       ##
echo ##                                                            ##
echo ##        ##### #   #  #### #####  ###  #     #               ##
echo ##          #   ##  # #       #   #   # #     #               ##
echo ##          #   # # #  ###    #   ##### #     #               ##
echo ##          #   #  ##     #   #   #   # #     #               ##
echo ##        ##### #   # ####    #   #   # ##### #####           ##
echo ##                                                            ##
echo ##                                                            ##
echo ##  brought to you by %AUTHORS%                        ##
echo ##                                                            ##
echo ##  %PROJECT%      ##
echo ##                                                            ##
echo ################################################################
echo.

echo Installing Fluent Bit version %FLUENTBIT_VERSION% container using image...
echo.

REM Check the  podman installation.
echo Checking if Podman is installed...
echo
call podman --version  

if %ERRORLEVEL% EQU 0 GOTO :containerStart
  echo Error occurred during Podman testing...
  echo.
  echo Podman is required but not installed yet... download and install: https://podman.io/getting-started/installation
  echo.
  GOTO :EOF

:containerStart
echo Starting the Fluent Bit container image...
echo
call podman run --name fb -ti cr.fluentbit.io/fluent/fluent-bit:%FLUENTBIT_VERSION%

if %ERRORLEVEL% EQU 0 GOTO :closingAscii
  echo 
  echo Cleaning up any Fluent Bit images that might still be running...
  echo.
  call podman container stop fb 
  call podman container rm fb 

  echo Starting fresh Fluent Bit container image...
  echo.
  call podman run --name fb -ti cr.fluentbit.io/fluent/fluent-bit:%FLUENTBIT_VERSION%

  if %ERRORLEVEL% EQU 0 GOTO :closingAscii
     echo.
     echo Error occurred during 'podman run' starting Fluent Bit container... make sure your Windows podman
     echo installation is initialized and working correctly before trying this installation script again.
     echo.
     GOTO :EOF

:closingAscii

REM produce the installation end report.
echo.
echo ============================================================================
echo =                                                                          =
echo =  Install complete, get ready to rock Fluent Bit!                         =
echo =                                                                          =
echo =  Fluent Bit was started interactively in a container, which you just     =
echo =  killed with CTRL-C. To restart, just run the following command:         =
echo =                                                                          =
echo =      C: podman run --name fb -ti cr.fluentbit.io/fluent/fluent-bit:%FLUENTBIT_VERSION%"  =
echo =                                                                          =
echo =  To get started building your first observability pipelines with real    =
echo =  hands-on examples, see below.                                           =
echo =                                                                          =
echo =  Getting started workshop available online:                              =
echo =  https://o11y-workshops.gitlab.io/workshop-fluentbit                     =
echo =                                                                          =
echo ============================================================================
echo.
GOTO :EOF
