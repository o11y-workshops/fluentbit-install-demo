Fluent Bit Easy Install
=======================
This demo is to install Fluent Bit, a cloud native data pipeline project, on your local machine from the supported
released version. It delivers a ready to use Fluent Bit installation. The installation scripts check for and validate
prerequisites automatically.

There are two options for installation detailed below.

Install in a container (Podman)
-------------------------------
This is an installation using the provided Fluent Bit container image. You will
run this container on a virtual machine provided by Podman.

**Prerequisites:** Podman 4.x+ with your podman machine started.

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/fluentbit-install-demo/-/archive/v1.4/fluentbit-install-demo-v1.4.zip)

2. Run 'init.sh' with the correct argument from the project root directory: 

``` 
   $ podman machine init
   $ ./init.sh podman
```

3. The Fluent Bit container image is now running, connect:

```
   $ podman attach fluentbit
```


Install on your local machine
--------------------------------
This is an installation from the source code of the Fluent Bit project. You will
test, build, and deploy Fluent Bitlocally on your machine.

**Prerequisites:** cmake, openssl, and bison v3+

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/fluentbit-install-demo/-/archive/v1.4/fluentbit-install-demo-v1.4.zip)

2. Run 'init.sh' or 'init.bat' file with the correct argument from the project root directory. Note 'init.bat' must be run with Administrative privileges.  

``` 
   $ ./init.sh source
   
   C: init.bat source
```

3. Fluent bit is now installed, test it by running the following from the
   install project root directory:

```
   # Example shown here of Fluent Bit version 3.1.4.
   # 
   $ ./target/fluent-bit-3.1.4/bin/fluent-bit/bin/fluent-bit --version

   Fluent Bit v3.1.4
```

Getting started
---------------
Not sure how to get started with Fluent Bit and building your first data
pipelines? Try this 
<a href="https://o11y-workshops.gitlab.io/workshop-fluentbit" target="_blank">online workshop:</a>

----------------------------------------------

<a href="https://o11y-workshops.gitlab.io/workshop-fluentbit" target="_blank"><img src="docs/demo-images/workshop.png" width="70%"></a>

----------------------------------------------

Notes:
-----
If for any reason the installation breaks or you want a new Fluent Bit installation, just rerun the installation script to
reinstall the Fluent Bit project in the target directory. 


Supporting Articles
-------------------
- [Telemetry Pipelines Workshop - Introduction to Fluent Bit](https://www.schabell.org/2024/03/telemetry-pipelines-workshop-introduction-to-fluent-bit.html)

- [Telemetry Pipelines Workshop - Installing Fluent Bit from Source](https://www.schabell.org/2024/03/telemetry-pipelines-workshop-installing-fluent-bit-from-source.html)

- [Telemetry Pipelines Workshop - Installing Fluent Bit in Container](https://www.schabell.org/2024/03/telemetry-pieplines-workshop-installing-fluent-bit-in-container.html)

- [Telemetry Pipelines Workshop - Building First Pipelines with Fluent Bit](https://www.schabell.org/2024/04/telemetry-pieplines-workshop-building-first-pipelines.html)

- [Telemetry Pipelines Workshop - Parsing Multiple Events with Fluent Bit](https://www.schabell.org/2024/04/telemetry-pipelines-workshop-parsing-multiple-events.html)

- [Telemetry Pipelines Workshop - Metric Collection Processing with Fluent Bit](https://www.schabell.org/2024/04/telemetry-pipelines-workshop-metric-collection-processing.html)

- [Telemetry Pipelines Workshop - Routing Events with Fluent Bit](https://www.schabell.org/2024/04/telemetry-pipelines-workshop-routing-events-with-fluentbit.html)

- [Telemetry Pipelines Workshop - Filtering Events with Fluent Bit](https://www.schabell.org/2024/04/telemetry-pipelines-workshop-filtering-events-with-fluentbit.html)

- [Telemetry Pipelines Workshop - Understanding Backpressure with Fluent Bit](https://www.schabell.org/2024/04/telemetry-pipelines-workshop-understanding-backpressure-with-fluentbit.html)

- [Telemetry Pipelines Workshop - Avoiding telemetry data loss with Fluent Bit](https://www.schabell.org/2024/05/telemetry-pipelines-workshop-avoiding-telemetry-data-loss-with-fluent-bit.html)

- [O11y Guide - Your First Steps in Cloud Native Observability](https://www.schabell.org/2022/09/o11y-guide-your-first-steps-in-cloud-native-observability.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.5 - Supporting Fluent Bit v3.2.2, installing in a container or from source.

- v1.4 - Supporting Fluent Bit v3.1.4, installing in a container or from source.

- v1.3 - Supporting Fluent Bit v3.0.7, installing in a container or from source.

- v1.2 - Supporting Fluent Bit v3.0.4, installing in a container or from source.

- v1.1 - Supporting Fluent Bit v3.0.1, installing in a container or from source.

- v1.0 - Supporting Fluent Bit v2.2.2, installing in a container or from source.

