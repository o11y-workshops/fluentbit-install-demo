#!/bin/bash

# Collection of helper functions used in demos.
#

function print_docs()
{
  echo "To use this installation script you have to provide one argument"
  echo "indicating how you want to install Fluent Bit. You have the option"
  echo "to install a container image or build it from source:"
	echo
	echo "   $ ./init.sh {podman|source}"
	echo
	echo "Both methods are validated by the install scripts."
	echo
}


function validate_build_mode()
{
  local mode=$1  # passed arg to this function.

  if [[ $mode == "podman" ]]; then
    echo "Installing container image..."
    echo
    MODE="podman"
  elif [[ $mode == "source" ]]; then
    echo "Installing from source..."
    echo
    MODE="source"
  else
    # bad arg passed.
    print_docs
    echo
    exit
  fi
}


function unpack_fluentbit_project() 
{
  echo "Unpacking Fluent Bit project into target directory..."
  echo
  command mkdir -p ./target

  if [ $? -ne 0 ]; then
  		echo "Error occurred during 'mkdir' command..."
  		echo
  		exit;
    fi

  command unzip ./installs/"${FLUENTBIT_SRC}" -d ./target 1> /dev/null

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred during 'unzip' command..."
		echo
		exit;
  fi

  echo "Fluent Bit project unpacked successfully into target directory!"
  echo
}


function install_fluentbit_in_container()
{
  # Check the  podman installation.
  echo "Checking if Podman is installed..."
  echo
  command -v podman --version -v  >/dev/null 2>&1 || { echo >&2 "Podman is required but not installed yet... download and install: https://podman.io/getting-started/installation"; exit; }


  echo "Starting the Fluent Bit container image..."
  echo
  command podman run --name fb -ti cr.fluentbit.io/fluent/fluent-bit:"${FLUENTBIT_VERSION}"

  if [ $? -ne 0 ]; then
		echo
    echo "Cleaning up any Fluent Bit images that might still be running..."
    echo
    command podman container stop fb >/dev/null 2>&1
    command podman container rm fb >/dev/null 2>&1

    echo "Starting fresh Fluent Bit container image..."
    echo
    command podman run --name fb -ti cr.fluentbit.io/fluent/fluent-bit:"${FLUENTBIT_VERSION}"

    if [ $? -ne 0 ]; then
      echo
      if [[ $(uname) == "Darwin" ]]; then
        echo "Error occurred during 'podman run' starting Fluent Bit container... make"
        echo "sure you have started the Podman machine as follows and rerun this"
        echo "installation script again:"
        echo
        echo "     $ podman machine start "
        echo
        exit;
      fi

      echo "Error occurred during 'podman run' starting Fluent Bit container... make sure your Linux podman installation"
      echo "is initialized and working correctly before trying this installation script again."
      exit;
    fi
  fi
}


function install_fluentbit_from_source()
{
  # Check for cmake installation.
  command -v cmake --version -v >/dev/null 2>&1 || { echo >&2 "cmake is required but not installed yet... install with Brew or your package manager before proceeding..."; echo; exit; }

  # Check for openssl installation.
  command -v openssl --version -v >/dev/null 2>&1 || { echo >&2 "openssl is required but not installed yet... install with Brew or your package manager before proceeding"; echo; exit; }

  # Check for bison installation.
  command -v bison --version -v >/dev/null 2>&1 || { echo >&2 "bison is required but not installed yet... install with Brew or your package manager before proceeding"; echo; exit; }

  # Remove the old Fluent Bit installation, if it exists.
  if [ -x "$FLUENTBIT_HOME" ]; then
    echo "  - removing existing installation directory..."
    echo
    rm -rf "${FLUENTBIT_HOME}"
  fi

  # just to be sure that Fluent Bit is not running from a previous source install.
  command killall fluentbit >/dev/null 2>&1

  # Version check for bison.
  echo "Checking for bison version needed to build:"
  maj_version=$(bison --version  | head -1 | cut -d " " -f4 | cut -d "." -f1)

  if [ "${maj_version}" -ge "${BISON_VERSION}" ]; then
    echo
    echo "Bison major version is good..."
    echo
  else
    echo "Your bison version must be ${BISON_VERSION} or higher..."
    echo
    exit;
  fi

  unpack_fluentbit_project

  echo "Moving to the Fluent Bit project build directory..."
  echo
  command cd "${FLUENTBIT_HOME}/build"

  if [ $? -ne 0 ]; then
		echo "Error occurred during 'cd' to Fluent Bit build directory command..."
		echo
		exit;
  fi

  echo "Building Fluent Bit (phase 1):"
  echo
  command cmake -DFLB_DEV=on -DCMAKE_INSTALL_PREFIX=${FLUENTBIT_HOME}/bin/fluent-bit ../

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred with 'cmake' command..."
		echo
		exit;
  fi
  echo
  echo "Phase 1 build successful, building continuing with phase 2:"
  echo
  command make -j 16

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred with 'make' command..."
		echo
		exit;
  fi
  echo
  echo "Phase 2 build successful, installing locally:"
  echo
  command make install 

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred with 'make install' command..."
		echo
		exit;
  fi

  echo
  echo "Fluent Bit built successfully!"
  echo
  echo "Copying to ${FLUENTBIT_HOME}/bin for use."
  echo
  command cp -r target/fluent-bit-$FLUENTBIT_VERSION/bin/fluent-bit ../bin/
}
