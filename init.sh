#!/bin/bash

DEMO="Fluent Bit Easy Install demo"
AUTHORS="Eric D. Schabell"
PROJECT="git@gitlab.com:o11y-workshops/fluentbit-install-demo.git"

# variables used globally in sourced functions.
export FLUENTBIT_VERSION=3.2.2
export FLUENTBIT_SRC=fluent-bit-${FLUENTBIT_VERSION}.zip
export FLUENTBIT_HOME=./target/fluent-bit-${FLUENTBIT_VERSION}
export BISON_VERSION=3
export MODE="none"

# importing functions.
source ./support/functions.sh

# wipe screen.
clear

echo
echo "#######################################################################"
echo "##                                                                   ##"   
echo "##  Setting up the ${DEMO}                      ##"
echo "##                                                                   ##"   
echo "##      #####  #     #   # ##### #   # #####   ####  ##### #####     ##"
echo "##      #      #     #   # #     ##  #   #     #   #   #     #       ##"
echo "##      ####   #     #   # ###   # # #   #     ####    #     #       ##"
echo "##      #      #     #   # #     #  ##   #     #   #   #     #       ##"
echo "##      #      ##### ##### ##### #   #   #     ####  #####   #       ##"
echo "##                                                                   ##"
echo "##                       #####  ###   #### #   #                     ##"
echo "##                       #     #   # #      # #                      ##"
echo "##                       ###   #####  ###    #                       ##"
echo "##                       #     #   #     #   #                       ##"
echo "##                       ##### #   # ####    #                       ##"
echo "##                                                                   ##"
echo "##               ##### #   #  #### #####  ###  #     #               ##"
echo "##                 #   ##  # #       #   #   # #     #               ##"
echo "##                 #   # # #  ###    #   ##### #     #               ##"
echo "##                 #   #  ##     #   #   #   # #     #               ##"
echo "##               ##### #   # ####    #   #   # ##### #####           ##"
echo "##                                                                   ##"   
echo "##  brought to you by ${AUTHORS}                               ##"
echo "##                                                                   ##"   
echo "##  ${PROJECT}         ##"
echo "##                                                                   ##"   
echo "#######################################################################"
echo

echo "Checking the build mode arguments..."
echo
# check for build mode argument.
if [ $# -eq 1 ]; then
  # passing argument to function.
  validate_build_mode $1
elif [ $# -gt 1 ]; then
  # too many parameters passed, show docs.
  print_docs
  echo
  exit
else
  # no parameters passed, show docs.
	print_docs
	echo
	exit
fi

if [[ $MODE == "podman" ]]; then
  echo "Installing Fluent Bit version ${FLUENTBIT_VERSION} container using image..."
  echo
  install_fluentbit_in_container
elif [[ $MODE == "source" ]]; then
  echo "Installing Fluent Bit version ${FLUENTBIT_VERSION} from the source project..."
  echo
  install_fluentbit_from_source
else
  echo "The wrong mode was passed to install Perses: $MODE"
  echo
  exit;
fi

if [[ $MODE == "source" ]]; then
  echo
  echo "========================================================="
  echo "=                                                       ="
  echo "=  Install complete, get ready to rock Fluent Bit!      ="
  echo "=                                                       ="
  echo "=  The binary build is available to run at:             ="
  echo "=                                                       ="
  echo "=  $ cd target/fluent-bit-${FLUENTBIT_VERSION}/bin/fluent-bit          ="
  echo "=  $ bin/fluent-bit --version                           ="
  echo "=                                                       ="
  echo "=   Fluent Bit v${FLUENTBIT_VERSION}                                   ="
  echo "=                                                       ="
  echo "=  To get started building your first observability     ="
  echo "=  observability pipelines with real hands on           ="
  echo "=  examples, see below.                                 ="
  echo "=                                                       ="
  echo "=  Getting started workshop available online:           ="
  echo "=  https://o11y-workshops.gitlab.io/workshop-fluentbit  ="
  echo "=                                                       ="
  echo "========================================================="
  echo

else
  echo
  echo "================================================================================="
  echo "=                                                                               ="
  echo "=  Install complete, get ready to rock Fluent Bit!                              ="
  echo "=                                                                               ="
  echo "=  Fluent Bit was started interactively in a container, which you just          ="
  echo "=  killed with CTRL-C. To restart, just run the following command:              ="
  echo "=                                                                               ="
  echo "=      $ podman run --rm --name fb -ti cr.fluentbit.io/fluent/fluent-bit:${FLUENTBIT_VERSION}  ="
  echo "=                                                                               ="
  echo "=  To get started building your first observability pipelines with real         ="
  echo "=  hands-on examples, see below.                                                ="
  echo "=                                                                               ="
  echo "=  Getting started workshop available online:                                   ="
  echo "=  https://o11y-workshops.gitlab.io/workshop-fluentbit                          ="
  echo "=                                                                               ="
  echo "================================================================================="
  echo
fi

